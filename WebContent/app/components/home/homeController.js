angular.module('testApp.controllers')
  .controller('HomeCtrl', function($scope, $cookies, $mdDialog, homeService){

    $scope.message = "";

    $scope.readFile = function($fileContent) {
      $scope.fileContent = $fileContent;
    };

    homeService.list().then(function(res) {
      if (res.status === 200){
        $scope.executions = res.data;
      } else {
        $scope.message = res.status;
      }
    })
    .catch(function (data) {
      $scope.message = data.status;
    });

    $scope.saveExecution = function() {
      $scope.fileContent = $scope.fileContent.split("\n");
      $scope.fileContent.pop(); //Remove empty val
      var values = $scope.fileContent.map(Number);

      var res = $scope.processFile(values);
      $scope.downloadFile(res);

      $scope.execution.exeDate = new Date();
      homeService.save($scope.execution.exeIdentification, $scope.execution.exeDate).then(function(res){
        if (res.status == 200) {
          $scope.executions.push($scope.execution);
          location.reload();
        } else {
          $scope.message = 'No se pudo guardar el registro, intenta nuevamente.'
        }
      });

    }

    $scope.processFile = function(values) {
      var T = 0;
      var sw = 0, count = 0, N = 0, i = 1;
      var weight = [];
      var res = "";

      while(i <= values.length +1 && T < values[0]) {
        N = values[i];
        weight = values.slice((i + 1), (i + N + 1));

        weight.sort(function(a, b){ return a-b });

        while ((weight.length > 0) && (sw == 0)) {
          var k = weight.length - 1;
          if (weight[k] >= 50) {
            weight.splice(k, 1);
            count++;
          } else if (weight[k] < 50) {
              var v = Math.floor(50 / weight[k]);
              if (v < weight.length) {
                  weight.splice(k, 1);
                  for (j = 0; j < v; j++) {
                      weight.splice(0, 1);
                  }
                  count++;
              } else {
                  sw = 1;
              }
          }
        }

        res = res + 'Case #' + (T + 1) + ': ' + count + "\n";
        sw = 0;
        count = 0;
        T++;
        i = i + N + 1;
      }

      return res;
    }

    $scope.downloadFile = function(data) {
      var filename = 'output.txt';
      var blob = new Blob([data], {type: 'text/plain'});
      if(window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveBlob(blob, filename);
      } else {
        var elem = window.document.createElement('a');
        elem.href = window.URL.createObjectURL(blob);
        elem.download = filename;
        document.body.appendChild(elem);
        elem.click();
        document.body.removeChild(elem);
      }
    }

    // Table params
    $scope.query = {
      order: 'exeIdentification',
      limit: 10,
      page: 1
    };
  }).directive('onReadFile', function ($parse) {
    	return {
    		restrict: 'A',
    		scope: false,
    		link: function(scope, element, attrs) {
                var fn = $parse(attrs.onReadFile);

    			element.on('change', function(onChangeEvent) {
    				var reader = new FileReader();

    				reader.onload = function(onLoadEvent) {
    					scope.$apply(function() {
    						fn(scope, {$fileContent:onLoadEvent.target.result});
    					});
    				};

    				reader.readAsText((onChangeEvent.srcElement || onChangeEvent.target).files[0]);
    			});
    		}
    	};
});;
