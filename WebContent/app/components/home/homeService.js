angular.module('testApp.services')
  .factory('homeService', function($http) {

    function postJson(route, data) {
      data = data || {};
      return $http({
        url: route,
        data: data,
        dataType: 'json',
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        }
      })
    }

    return {
      list: function() {
        return $http.get('http://localhost:8080/testTNS-back/executions/list');
      },
      save : function(id, date){
        return postJson('http://localhost:8080/testTNS-back/executions/save',
                        { exeIdentification : id, exeDate : date })
      }
    };
  });
