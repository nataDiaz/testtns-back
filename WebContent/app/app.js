angular.module('testApp', ['ngRoute', 'ngMaterial', 'ngMessages', 'ngCookies',
  'testApp.controllers', 'testApp.services', 'md.data.table'])
    .config(function($routeProvider){
      $routeProvider
        .when('/', {
          templateUrl: 'app/components/home/home.html',
          controller: 'HomeCtrl'
        });
    });

angular.module('testApp.controllers', []);
angular.module('testApp.services', []);
