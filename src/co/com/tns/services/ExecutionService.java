/**
 * 
 */
package co.com.tns.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import co.com.tns.beans.Execution;
import co.com.tns.dao.ExecutionDAO;

/**
 * @author Natalia Diaz
 *
 */

@Path("/executions")
public class ExecutionService {

	ExecutionDAO executionDAO = new ExecutionDAO();

	/**
	 * 
	 */
	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Execution> listExecutions() {
		return executionDAO.getExecutions();
	}

	@POST
	@Path("/save")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Integer saveExecution(Execution execution) {
		return executionDAO.saveExecution(execution);
	}
}
