/**
 * 
 */
package co.com.tns.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import co.com.tns.beans.Execution;
import co.com.tns.utils.HibernateUtil;

/**
 * @author Natalia Diaz
 *
 */
public class ExecutionDAO {

	public List<Execution> getExecutions() {
		Session session = HibernateUtil.getSession();
		Transaction tx = null;
		List<Execution> executions = new ArrayList<>();

		try {
			Criteria c = session.createCriteria(Execution.class);
			tx = session.beginTransaction();

			executions = c.list();

			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

		return executions;
	}

	public Integer saveExecution(Execution execution) {
		Session session = HibernateUtil.getSession();
		Transaction tx = null;
		Integer exeId = null;

		try {
			tx = session.beginTransaction();
			exeId = (Integer) session.save(execution);

			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return exeId;
	}
}
