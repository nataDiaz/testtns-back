/**
 * 
 */
package co.com.tns.utils;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * @author Natalia Diaz
 *
 */
public class HibernateUtil {

	private static HibernateUtil instance = new HibernateUtil();
	private SessionFactory sessionFactory;

	public static HibernateUtil getInstance() {
		return instance;
	}

	private HibernateUtil() {
		Configuration configuration = new Configuration();
		configuration.configure("hibernate.cfg.xml");

		sessionFactory = configuration.buildSessionFactory();
	}

	public static Session getSession() throws HibernateException {
		Session session = getInstance().sessionFactory.openSession();

		return session;
	}
}