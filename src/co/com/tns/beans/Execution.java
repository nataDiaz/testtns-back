/**
 * 
 */
package co.com.tns.beans;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Natalia Diaz
 *
 */
@Entity
@Table(name = "tblExecutions")
public class Execution implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8907265182694941425L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer exeId;
	@Column(name = "exeIdentification")
	private String exeIdentification;
	@Column(name = "exeDate")
	@Temporal(TemporalType.TIMESTAMP)
	private Date exeDate;

	public Execution() {
	}

	public Execution(Integer exeId, String exeIdentification, Date exeDate) {
		this.exeId = exeId;
		this.exeIdentification = exeIdentification;
		this.exeDate = exeDate;
	}

	public Integer getExeId() {
		return exeId;
	}

	public void setExeId(Integer exeId) {
		this.exeId = exeId;
	}

	public String getExeIdentification() {
		return exeIdentification;
	}

	public void setExeIdentification(String exeIdentification) {
		this.exeIdentification = exeIdentification;
	}

	public Date getExeDate() {
		return exeDate;
	}

	public void setExeDate(Date exeDate) {
		this.exeDate = exeDate;
	}
}
