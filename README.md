# testTNS #

AngularJS/Java web app with Maven, Jersey REST, Hibernate and MySQL, to see how much lazy is Wilson and save executions.

## Set up ##

* ### Database ###

Create database `testTNS` on your MySQL server. Default credentials are `root` for user and password, you can change this on hibernate.cfg.xml file, on the root directory.

* ### Backend ###

Open the project on the IDE and run Maven update project, to download dependencies. Then run on server Apache Tomcat (i used v9.0). After that, services should be exposed on `localhost:8080`.

* ### Frontend ###

Go to WebContent folder and run:

```
$ npm install
$ http-server -o
```

Those steps download AngularJS dependencies and open a new window on your browser with the webapp. Now you can test uploading a test file and see the results.


### Thanks :) ###